﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    abstract class  Item
    {
        protected float _cost;
        protected string _name;
        
        public abstract void Write();
        public abstract float Cost();
    }
}
