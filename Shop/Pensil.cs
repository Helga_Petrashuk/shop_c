﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class Pensil: Uneatable
    {
        public Pensil()
        {
            _name = "Parker";
            _cost = 5.7f;
        }

        public Pensil(string name, float cost)
        {
            _name = name;
            _cost = cost;
        }
        public void Drow ()
        {
            Console.WriteLine("This pensil drowing");
        }
        public override void Write()
        {
            Console.WriteLine($"Pensil: {_name}, cost: {_cost} $");
        }
        public override float Cost()
        {
            return _cost;
        }
    }
}
