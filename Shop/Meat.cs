﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class Meat: Eatable
    {
        public Meat()
        {
            _name = "Chiken";
            _expiration = 5;
            _cost = 12.6f;
        }

        public Meat (string name, float cost, int expiration)
        {
            _name = name;
            _cost = cost;
            _expiration = expiration;
        }

        public override void Write ()
        {
            Console.WriteLine($"Meat: {_name}, cost {_cost}, expiration: {_expiration} day");
        }
        public override float Cost()
        {
            return _cost;
        }
    }
}
