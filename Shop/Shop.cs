﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class Shop
    {
        private Item[] product;
        public Shop (Item[] product)
        {
            this.product = product;
        }

        public Shop ()
        {
            product = new Item[6];
        }

        public void AddProduct ()
        {
            product[0] = new Icecream();
            product[1] = new Meat("Говядина", 12.50f, 3);
            product[2] = new Icecream(IceCreamType.Raspberries, 2.53f, "Solletto", 18);
            product[3] = new Watch();
            product[4] = new Pensil("Parker", 215f);
            product[5] = new Watch("Miband", 150)
        }

        public  void ShowEatalbe()
        {
            int count = 0;
            for(int i =0; i < product.Length; i++)
            {
                if (product[i] is Eatable)
                {
                    count++;
                    product[i].Write();
                }
            }
            Console.WriteLine();
            Console.WriteLine($"There are {count} edible products in the store");
        }

        public void ShowUneatable ()
        {
            int count = 0;
            Pensil pencil = new Pensil();
            Watch watch = new Watch();
            for (int i = 0; i < product.Length; i++)
            {
                if (product[i] is Uneatable)
                {
                    count++;
                    product[i].Write();
                }
            }
            Console.WriteLine();
            Console.WriteLine($"there are {count} uneatable products in the store");
        }

        public void ShowDearestItem()
        {
            Console.Write($"Most expensive product is ");
            float cost=0;
            for (int i=0; i<product.Length; i++) 
            {
                float _cost = product[i].Cost();
                if (cost < _cost)
                { cost = _cost; }
            }
            for (int i=0; i<product.Length; i ++)
            {
                if (cost == product[i].Cost()) 
                {
                    product[i].Write();
                }
            }
            
        }

        public void SwowCheapectItem()
        {
            Console.Write($"Most cheep product is ");
            float cost= product[0].Cost();
            for (int i = 0; i < product.Length; i++)
            {
                float _cost = product[i].Cost();
                if (cost > _cost)
                { cost = _cost; }
            }
            for (int i = 0; i < product.Length; i++)
            {
                if (cost == product[i].Cost())
                {
                    product[i].Write();
                }
            }
        }
    }
}
