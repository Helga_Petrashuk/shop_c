﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    abstract class Uneatable: Item
    {
        public abstract override void Write();
        public abstract override float Cost();
    }
}
