﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class Watch : Uneatable
    {
        public Watch()
        {
            _name = "Luch";
            _cost = 16;
        }
        
        public Watch( string name, float cost)
        {
            _name = name;
            _cost = cost;
        }

        public override void Write()
        {
            Console.WriteLine($"Watch {_name}, cost {_cost}$");
        }

        public void WriteTime()
        {
            Console.WriteLine(DateTime.Now);
        }

        public override float Cost()
        {
            return _cost;
        }

    }
}
