﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    class Icecream : Eatable
    {
        IceCreamType type;

        public Icecream ()
        {
            type = IceCreamType.Chocolate;
            _name = "Top";
            _cost = 1.58f;
            _expiration = 12;
        }
        public Icecream (IceCreamType type, float cost, string name, int expiration)
        {
            this.type = type;
            _cost = cost;
            _name = name;
            _expiration = expiration;
        }

        public override void Write()
        {
            string icetype;
            if (type == IceCreamType.Chocolate)
            {
                icetype = "Chocolate";
            }
            else
            {
                icetype = "Raspberries";
            }
            Console.WriteLine($"IceCream  {_name}, type {icetype}, cost {_cost}$, expiration {_expiration} month");
        }

        public override float Cost()
        {
            return _cost;
        }
    }

    enum IceCreamType
    {
        Raspberries,
        Chocolate
    }
}
