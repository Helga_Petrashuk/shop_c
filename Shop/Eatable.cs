﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop
{
    abstract class Eatable : Item
    {
        protected int _expiration;
        Meat meat;
        Icecream iceCream;
        public abstract override void Write();
        public abstract override float Cost();
    }
}
