﻿using System;

namespace Shop
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleKeyInfo key;
            Shop shop = new Shop();
            shop.AddProduct();
            do
            {
                key = Console.ReadKey();
                Console.WriteLine();
                
                
                switch(key.Key)
                {
                    case ConsoleKey.D1:
                        Console.Clear();
                        shop.ShowEatalbe();
                        break;
                    case ConsoleKey.D2:
                        Console.Clear();
                        shop.ShowUneatable();
                        break;
                    case ConsoleKey.D3:
                        Console.Clear();
                        shop.SwowCheapectItem();
                        break;
                    case ConsoleKey.D4:
                        Console.Clear();
                        shop.ShowDearestItem();
                        break;
                    default:
                        break;

                }
            } while (key.Key != ConsoleKey.Escape);
        }
    }
}
